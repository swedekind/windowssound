from unittest import TestCase

from argument_controller import ArgumentController


class TestArgumentController(TestCase):

    def setUp(self) -> None:
        self.sut = ArgumentController()

    def test_parse_arguments_throws_when_volume_is_valid_int(self):
        self.assertRaises(SystemExit, self.sut.parse_arguments, '-v abc'.split())

    def test_parse_arguments_returns_volume_when_param_is_valid_integer(self):
        # Act
        result = self.sut.parse_arguments('-v 50'.split())

        # Assert
        self.assertEqual(result.volume, 50)

    def test_parse_arguments_indicates_notification_flag_when_parameter_is_set(self):
        # Act
        result = self.sut.parse_arguments('-n'.split())

        # Assert
        self.assertEqual(result.notification, False)

    def test_parse_arguments_indicates_mute_flag_when_parameter_is_set(self):
        # Act
        result = self.sut.parse_arguments('-m'.split())

        # Assert
        self.assertEqual(result.mute, True)

    def test_parse_arguments_indicates_unmute_flag_when_parameter_is_set(self):
        # Act
        result = self.sut.parse_arguments('-u'.split())

        # Assert
        self.assertEqual(result.unmute, True)

    def test_parse_arguments_throws_when_mute_and_unmute_flags_are_set_at_same_time(self):
        self.assertRaises(SystemExit, self.sut.parse_arguments, '-u -m'.split())
