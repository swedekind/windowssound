from unittest import TestCase
from unittest.mock import MagicMock, patch

# https://docs.python.org/3/library/unittest.mock.html
from volume_controller import VolumeController


class TestVolumeController(TestCase):

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_is_muted_should_return_true_when_audio_volume_get_mute_returns_1(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.GetMute = MagicMock(return_value=1)

        # Act
        muted = sut.is_muted()

        # Assert
        self.assertTrue(muted)

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_is_muted_should_return_false_when_audio_volume_get_mute_returns_0(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.GetMute = MagicMock(return_value=0)

        # Act
        muted = sut.is_muted()

        # Assert
        self.assertFalse(muted)

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_unmute(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.SetMute = MagicMock()

        # Act
        sut.unmute()

        # Assert
        mocked_audio_volume.SetMute.assert_called_once_with(0, None)

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_mute(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.SetMute = MagicMock()

        # Act
        sut.mute()

        # Assert
        mocked_audio_volume.SetMute.assert_called_once_with(1, None)

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_set_volume_in_percent(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.SetMasterVolumeLevelScalar = MagicMock()

        # Act
        sut.volume_in_percent = 65

        # Assert
        mocked_audio_volume.SetMasterVolumeLevelScalar.assert_called_once_with(0.65, None)

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_set_volume_in_percent_ignores_call_when_parameter_below_zero(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.SetMasterVolumeLevelScalar = MagicMock()

        # Act
        sut.volume_in_percent = -10

        # Assert
        mocked_audio_volume.SetMasterVolumeLevelScalar.assert_not_called()

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_set_volume_in_percent_ignores_call_when_parameter_greater_than_one_hundred(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.SetMasterVolumeLevelScalar = MagicMock()

        # Act
        sut.volume_in_percent = 110

        # Assert
        mocked_audio_volume.SetMasterVolumeLevelScalar.assert_not_called()

    @patch('pycaw.pycaw.IAudioEndpointVolume')
    def test_get_volume_in_percent(self, mocked_audio_volume):
        # Arrange
        sut = VolumeController(mocked_audio_volume)
        mocked_audio_volume.GetMasterVolumeLevelScalar = MagicMock(return_value=0.3)

        # Act
        percent = sut.volume_in_percent

        # Assert
        mocked_audio_volume.GetMasterVolumeLevelScalar.assert_called_once_with()
        self.assertEqual(percent, 30)
