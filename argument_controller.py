from argparse import ArgumentParser, Namespace


class Arguments(Namespace):
    volume: int
    no_notification: bool
    mute: bool
    unmute: bool


class ArgumentController:
    __parser: ArgumentParser

    def __init__(self):
        parser = ArgumentParser(description='Manage windows volume in console')
        parser.add_argument('-n', '--notification', action='store_false', help='Show windows notification')

        group = parser.add_mutually_exclusive_group()
        group.add_argument('-v', '--volume', type=int, help='The volume in percent')
        group.add_argument('-m', '--mute', action='store_true', help='Mute volume')
        group.add_argument('-u', '--unmute', action='store_true', help='Unmute volume')

        self.__parser = parser

    def parse_arguments(self, args=None) -> Arguments:
        if args is None:
            # noinspection PyTypeChecker
            return self.__parser.parse_args()
        else:
            # noinspection PyTypeChecker
            return self.__parser.parse_args(args)
