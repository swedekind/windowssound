from pycaw.pycaw import IAudioEndpointVolume


class VolumeController:
    __audio_volume: IAudioEndpointVolume = None

    def __init__(self, audio_volume: IAudioEndpointVolume):
        self.__audio_volume = audio_volume

    @property
    def volume_in_percent(self) -> int:
        # noinspection PyUnresolvedReferences
        return round(self.__audio_volume.GetMasterVolumeLevelScalar() * 100)

    @volume_in_percent.setter
    def volume_in_percent(self, percent: int):
        if 0 <= percent <= 100:
            new_volume = percent / 100
            # noinspection PyUnresolvedReferences
            self.__audio_volume.SetMasterVolumeLevelScalar(new_volume, None)

    def mute(self):
        # noinspection PyUnresolvedReferences
        self.__audio_volume.SetMute(1, None)

    def unmute(self):
        # noinspection PyUnresolvedReferences
        self.__audio_volume.SetMute(0, None)

    def is_muted(self) -> bool:
        # noinspection PyUnresolvedReferences
        return self.__audio_volume.GetMute() == 1
