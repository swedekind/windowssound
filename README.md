# Python Venv reminder
Enable `venv` in console
> .\venv\Scripts\activate

Disable `venv`
>deactivate

# Links
* https://github.com/AndreMiras/pycaw
* https://docs.microsoft.com/en-us/windows/win32/coreaudio/core-audio-apis-in-windows-vista
* https://docs.microsoft.com/en-us/windows/win32/api/_coreaudio/