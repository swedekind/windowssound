$scriptDir = split-path $PSCommandPath -Parent
$applicationDir = "$env:LocalAppData\winsound"
Remove-Item -Force -Recurse -ErrorAction Ignore $applicationDir
New-Item -ItemType Directory $applicationDir
Copy-Item -Recurse $scriptDir\dist\winsound\* $applicationDir