from ctypes import cast, POINTER

from comtypes import CLSCTX_ALL
from pycaw.pycaw import AudioUtilities, IAudioEndpointVolume
from win10toast import ToastNotifier

from argument_controller import ArgumentController, Arguments
from volume_controller import VolumeController


def create_volume_controller() -> VolumeController:
    global argument_controller
    devices = AudioUtilities.GetSpeakers()
    interface = devices.Activate(IAudioEndpointVolume._iid_, CLSCTX_ALL, None)
    volume = cast(interface, POINTER(IAudioEndpointVolume))
    return VolumeController(volume)


def show_toast(args: Arguments, message: str) -> None:
    if not args.notification:
        toaster = ToastNotifier()
        toaster.show_toast(title="Windows Sound CMD",
                           msg=message,
                           duration=3)


argument_controller = ArgumentController()
arguments = argument_controller.parse_arguments()
volume_controller = create_volume_controller()

if arguments.volume:
    volume_controller.volume_in_percent = arguments.volume
    show_toast(arguments, f'Lautstärke auf {arguments.volume} eingestellt.')
elif arguments.mute:
    volume_controller.mute()
    show_toast(arguments, 'Ton deaktiviert.')
elif arguments.unmute:
    volume_controller.unmute()
    show_toast(arguments, 'Ton aktiviert.')
