Write-Output "######################################################################"
Write-Output "#Preparing...                                                        #"
Write-Output "######################################################################"
Remove-Item -Force -Recurse -ErrorAction Ignore .\build
Remove-Item -Force -Recurse -ErrorAction Ignore .\__pycache__
Remove-Item -Force -Recurse -ErrorAction Ignore .\dist


Write-Output "######################################################################"
Write-Output "#Building                                                            #"
Write-Output "######################################################################"
.(".\venv\Scripts\activate.ps1")
.\venv\Scripts\pyinstaller.exe --clean --noupx --windowed --distpath .\dist\windowed --name winsoundw --additional-hooks-dir=hooks main.py
.\venv\Scripts\pyinstaller.exe --clean --noupx --console --distpath .\dist\console --name winsound --additional-hooks-dir=hooks main.py
deactivate


Write-Output "######################################################################"
Write-Output "#Package...                                                          #"
Write-Output "######################################################################"
Copy-Item .\dist\console\winsound\winsound.exe .\dist\windowed\winsoundw\winsound.exe
New-Item -ItemType Directory .\dist\winsound
Copy-Item -Recurse .\dist\windowed\winsoundw\* .\dist\winsound


Write-Output "######################################################################"
Write-Output "#Cleaning...                                                         #"
Write-Output "######################################################################"
Remove-Item -Force -Recurse -ErrorAction Ignore .\dist\console
Remove-Item -Force -Recurse -ErrorAction Ignore .\dist\windowed
Remove-Item -Force -Recurse -ErrorAction Ignore .\build
Remove-Item -Force -Recurse -ErrorAction Ignore .\__pycache__
Remove-Item -Force -ErrorAction Ignore .\*.spec